// Прочитать темы Циклов и Массивов(только то что мы прошли первые 3)
// --------------------------------------------------------------------
// ____________________________________________________________________

// Выведите столбец чисел от 1 до 100.
// let i = 1;
// while (i <= 100) {
//     console.log(i);
//     i++;
// } 


//  Выведите столбец чисел от 11 до 33.
// let i = 11;
// while (i <= 33) {
//     console.log(i);
//     i++;
// } 

//  Выведите столбец четных чисел в промежутке от 0 до 100.
// let i = 1;
// while (i <= 100) {
//     if (i % 2 === 0){
//     console.log(i);
// }    i++;
// } 

//  С помощью цикла найдите сумму чисел от 1 до 100.

// let i = 0;
// let sum = 0;
// for (let i = 0; i <= 100; i++) {
//     sum  += i;
// }
// console.log(sum)

// Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for выведите все эти элементы на экран.

// const numbers = [1, 2, 3, 4, 5];
// const result = [];

// for (let i = numbers.length -1; i >= 0; i--) {
//     const element = numbers[i];
// }

// console.log('result: ' + numbers)

//  Дан массив с элементами [1, 2, 3, 4, 5]. С помощью цикла for найдите сумму элементов этого массива. Запишите ее в переменную result.
// const numbers = [1, 2, 3, 4, 5];
// let sum = 0;

// for (let i = 0; i < numbers.length; i++) {
//     sum += numbers[i];
// }
// console.log(sum);

// Дан массив с элементами 2, 5, 9, 15, 0, 4. С помощью цикла for и оператора if выведите на экран столбец тех элементов массива, которые больше 3-х, но меньше 10.

// const nums = [2, 5, 9, 15, 0, 4];
// const result = [];

// for (let index = 0; index < nums.length; index++) {
//     const element = nums[index];

//     if (element > 3 && element < 10) {
//         result.push(element);
//     }
// }
// console.log(result);   

//  Дан массив с числами. Числа могут быть положительными и отрицательными. 
//Найдите сумму положительных элементов массива.
// const nums = [2, 2, 2, -15, 0, -4];
// const result = [];

// for (let index = 0; index < nums.length; index++) {
//     const element = nums[index];

//     if (element > 0) {
//         result.push(element);
//     }

//     sum = 0;
//     for (let i = 0; i < result.length; i++) {
//             sum += result[i];
//         }
// }
// console.log(sum);   

//  Дан массив с элементами 1, 2, 5, 9, 4, 13, 4, 10. 
// С помощью цикла for и оператора if проверьте есть ли в массиве элемент со значением, равным 4. 
// Если есть - выведите на экран 'Есть!' и выйдите из цикла. Если нет - ничего делать не надо.

// const nums = [1, 2, 5, 9, 4, 13, 4, 10];
// const result = [];

// for (let index = 0; index < nums.length; index++) {
//     const element = nums[index];

//     if (element === 4) {
//         console.log('Есть!');   
//     }
// }


// Дан массив с элементами 1, 2, 3, 4, 5, 6, 7, 8, 9. 
// С помощью цикла for создайте строку '-1-2-3-4-5-6-7-8-9-'

// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// let result = "-";

// for (let i = 0; i < numbers.length; i++) {
//     result += "" + numbers[i] + "-";
// }
// console.log(result)

// Задание *
// Напишите функцию которая принимает число больше 0 и выводит в консоли ёлочку.

// При заданном числе 5

// Результат должен получиться такой:

// #

// ##

// ###

// ####

// #####


//На switch-case

//#1 Переменная num может принимать 4 значения: 1, 2, 3 или 4. Если она имеет значение '1', 
//то в переменную result запишем 'зима', если имеет значение '2' – 'весна' и так далее. Решите задачу через switch-case.

// const season = 4;
// switch (season) {
//     case 1:
//         console.log("Winter");
//         break;

//         case 2:
//         console.log("Spring");
//         break;

//         case 3:
//         console.log("Summer");
//         break;

//         case 4:
//         console.log("Autumn");
//         break;

//     default:
//         break;
// }

