//#1
function run1() {
  const a = 10;
  const b = 3;
  const unit = a % b;

  console.log(
    "#1 Даны переменные a = 10 и b = 3. Найдите остаток от деления a на b."
  );
  console.log("Result is =", unit);
}

//#2
function run2() {
  const a = 10;
  const b = 3;
  const unit = a % b;
  const positive = a / b;
  console.log(
    "#2 Даны переменные a и b. Проверьте, что a делится без остатка на b. Если это так - выведите 'Делится' и результат деления, иначе выведите 'Делится с остатком' и остаток от деления."
  );
  if (unit === 0) {
    console.log("Делится. Ваш ответ = ", positive);
  } else console.log("Делится с остатком. Остаток от деления =", unit);
}

//#3
function run3() {
  const a = 2;
  const b = 10;
  const st = Math.pow(a, b);
  console.log(
    "#3 Возведите 2 в 10 степень. Результат запишите в переменную st."
  );
  console.log(`${a} в степени ${b} будет ${st}`);
}
//#4
function run4() {
  const a = 245;
  const root = Math.sqrt(a);
  console.log("#4 Найдите квадратный корень из 245.");
  console.log(`квадратный корень из ${a} будет ${root}`);
}

//#5
function run5() {
  const array = [4, 2, 5, 19, 13, 0, 10];
  let arraySum = [];
  console.log(
    "#5 Дан массив с элементами 4, 2, 5, 19, 13, 0, 10. Найдите квадратный корень из суммы кубов его элементов. Для решения воспользуйтесь циклом for."
  );
  for (let index = 0; index < array.length; index++) {
    arraySum += Math.pow(array[index], 3);
  }
  let root = Math.sqrt(arraySum);
  console.log(`квадратный корень из ${arraySum} будет ${root}`);
}
//#6
function run6() {
  const number = 379;
  let result = Math.sqrt(number);
  console.log(
    "Найдите квадратный корень из 379. Результат округлите до целых, до десятых, до сотых."
  );
  console.log(
    `Integer is ${Math.round(result)}. Tenth is ${result.toFixed(
      2
    )}. Hundredth is ${result.toFixed(3)}`
  );
}
//#7
function run7() {
  const number = 587;
  let result = Math.sqrt(number);
  let value1 = Math.floor(result);
  let value2 = Math.ceil(result);
  let newObject = {
    floor: value1,
    ceil: value2,
  };
  console.log(
    "#7 Найдите квадратный корень из 587. Округлите результат в большую и меньшую стороны, запишите результаты округления в объект с ключами 'floor' и 'ceil'."
  );
  console.log(newObject);
}

//#8
function run8() {
  const numbers = [4, -2, 5, 19, -130, 0, 10];
  let maximal = Math.max.apply(Math, numbers);
  let minimal = Math.min.apply(Math, numbers);
  console.log(
    "#8 Даны числа 4, -2, 5, 19, -130, 0, 10. Найдите минимальное и максимальное число."
  );
  console.log(`Maximal is ${maximal}, minimal is ${minimal}`);
}
//#9
function run9() {
  let random = Math.floor(Math.random() * 101);
  console.log("#9 Выведите на экран случайное целое число от 1 до 100.");
  console.log('Ваше случайное число - ', random);
}

//#10
function run10() {
  const array = [];

  for (let index = 0; index < 10; index++) {
    const element = Math.floor(Math.random() * 101);
    array.push(element);
  }
  console.log(
    "#10 Заполните массив 10-ю случайными целыми числами. (Подсказка: нужно воспользоваться циклами for или while)."
  );
  console.log(array);
}
//#11
function run11() {
  let a = -2;
  let b = 5;
  let result = a - b;
  let newResult = Math.abs(result);
  console.log(
    "#11 Даны переменные a и b. Найдите найдите модуль разности a и b. Проверьте работу скрипта самостоятельно для различных a и b."
  );
  console.log(`Module of difference of ${a} and ${b} is ${newResult}.`);
}
//#12
function run12() {
  let a = 1;
  let b = 6;
  let c = a - b;
  let newResult = Math.abs(c);
  console.log(
    "#12 Даны переменные a и b. Отнимите от a переменную b и результат присвойте переменной c. Сделайте так, чтобы в любом случае в переменную c записалось положительное значение. Проверьте работу скрипта при a и b, равных соответственно 3 и 5, 6 и 1."
  );
  console.log(`Result is ${newResult}.`);
}

//#13
function run13() {
  let array = [12, 15, 20, 25, 59, 79];
  let sum = 0;
  for (let index = 0; index < array.length; index++) {
    sum += array[index];
  }
  let result = sum / array.length;
  console.log(
    "#13 Дан массив arr. Найдите среднее арифметическое его элементов. Проверьте задачу на массиве с элементами 12, 15, 20, 25, 59, 79."
  );
  console.log(`Middle arifmetical of ${sum} is ${result}`);
}

//#14
function factorial(n) {
  let answer = 1;
  if (n == 0 || n == 1) {
    return answer;
  } else {
    for (let index = n; index >= 1; index--) {
      answer = answer * index;
    }
    return answer;
  }
}

//#15
let btnGet = document.getElementById("getProducts");
let myTable = document.querySelector("#table");
let footballers = [
  {
    name: "Alisson Bekker",
    number: "1",
    position: "Goalkeeper",
    nationality: "Brazilian",
    price: 50,
  },
  {
    name: "Virgil van Dijk",
    number: "4",
    position: "Centre-Back",
    nationality: "Netherlands",
    price: 55,
  },
  {
    name: "Joel Matip",
    number: "32",
    position: "Centre-Back",
    nationality: "Cameroon",
    price: 18,
  },
  {
    name: "Andrew Robertson",
    number: "26",
    position: "Left-Back",
    nationality: "Scotland",
    price: 65,
  },
  {
    name: "Trent Alexander-Arnold",
    number: "66",
    position: "Right-Back",
    nationality: "England",
    price: 80,
  },
  {
    name: "Fabinho",
    number: "3",
    position: "Defensive Midfield",
    nationality: "Brazilian",
    price: 60,
  },
  {
    name: "Thiago",
    number: "6",
    position: "Central Midfield",
    nationality: "Brazilian",
    price: 20,
  },
  {
    name: "Alex Oxlade-Chamberlain",
    number: "15",
    position: "Central Midfield",
    nationality: "England",
    price: 16,
  },
  {
    name: "Jordan Henderson (C)",
    number: "14",
    position: "Central Midfield",
    nationality: "England",
    price: 15,
  },
  {
    name: "Mohamed Salah",
    number: "11",
    position: "Right Winger",
    nationality: "Egypt",
    price: 90,
  },
  {
    name: "Darwin Núñez",
    number: "27",
    position: "Centre-Forward",
    nationality: "Uruguay",
    price: 55,
  },
  {
    name: "Roberto Firmino",
    number: "9",
    position: "Centre-Forward",
    nationality: "Brazilian",
    price: 32,
  },
];
let headersSorted = [
  "Name",
  "Number",
  "Position",
  "Nationality",
  "Price, mln €",
];
btnGet.addEventListener("click", () => {
  
  if (myTable.childNodes.length !== 0) {
    return;
  }

  let table = document.createElement("table");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  table.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    table.appendChild(row);
  });
  myTable.appendChild(table);
  console.log(myTable.childNodes);
});

//#16
function run16() {
  const array = [1, 2, 3, 4, 5];
  const object = array.reduce((accumulator, value) => {
    return { ...accumulator, [value]: value * 100 };
  }, {});
  console.log(
    `#16 Дан массив [1, 2, 3, 4, 5] с помощью метода reduce сделать объект в котором ключ будет равен элементу массива а значение (элемент * 100)
        input [1, 2, 3, 4, 5]
        output {1: 100, 2: 200, 3: 300 ...}1`
  );
  console.log(object);
}

//#17
function run17() {
  const array = [
    "Charge of Darkness",
    "Bulldoze",
    "Greater Bash",
    "Nether Strike",
    `Fortune's End`,
    `Fate's Edict`,
    "Purifying Flames",
    "Rain of Destiny",
    "False Promise",
    "Wraithfire Blast",
    "Mortal Strike",
  ];
  const object = array.reduce((accumulator, value) => {
    return { ...accumulator, [value]: Math.ceil(Math.random() * 101) };
  }, {});
  console.log(
    `#17 Дан массив skills = ['skill1', 'skill2', ... 'skilln'] скиллы произвольные самим придумать. Написать функцию которая создает из массива skills объект с рэндомными значениями от 1 до 100, использовать reduce
    ['hook', 'blackhole']
    {'hook': 20, 'blackhole: 100'}`
  );
  console.log(object);
}

//#18
function run18() {
  const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 100, 200, 300];

  function getOddEven(array) {
    return array.reduce(
      (accumulator, number) =>
        number % 2 === 0
          ? { odd: accumulator.odd, even: accumulator.even + number }
          : { even: accumulator.even, odd: accumulator.odd + number },
      { even: 0, odd: 0 }
    );
  }

  console.log(`Дан произвольный массив из чисел больше 0 с помощью reduce посчитать сумму четных и нечетных чисел.
    input [1,2,3,4,5,6,7,8,9,10,11, 100, 200, 300]
    output: { chet: 100, nechet: 100 } // 100 это для примера сумма должна высчитываться.`);
  console.log(getOddEven(array));
}

//#19
// declare variables - array is used from task 15
let btnGetSorted = document.getElementById("getProductsSorted");
let btnSort = document.getElementById("getSorted");
let myTableSorted = document.querySelector("#table-sorted");
let headers = ["Name", "Number", "Position", "Nationality", "Price, mln €"];

// When clicking button - return the table and sorting buttons.
btnGetSorted.addEventListener("click", () => {
  if (myTableSorted.childNodes.length !== 0) {
    return;
  }

  document.getElementById("sortContainer").style.display = "flex";
  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
});

// Sorting by name
getSortedAz.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > a.name) {
      return 1;
    }
    return 0;
  });
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

getSortedZa.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.reverse();
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

// Sorting by number
numberSortedAz.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.sort((a, b) => {
    return a.number - b.number;
  });
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

numberSortedZa.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.reverse();
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

// Sorting by position
posSortedAz.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.sort((a, b) => {
    if (a.position < b.position) {
      return -1;
    }
    if (a.position > a.position) {
      return 1;
    }
    return 0;
  });
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

posSortedZa.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.reverse();
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

// Sorting by Country
natSortedAz.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.sort((a, b) => {
    if (a.nationality < b.nationality) {
      return -1;
    }
    if (a.nationality > a.nationality) {
      return 1;
    }
    return 0;
  });
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

natSortedZa.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.reverse();
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

// Sorting by market value
priceSortedAz.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.sort((a, b) => {
    return a.price - b.price;
  });
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

priceSortedZa.addEventListener("click", () => {
  myTableSorted.firstChild.remove();

  footballers.reverse();
  console.log(footballers);

  let tableSorted = document.createElement("tableSorted");
  let headerRow = document.createElement("tr");
  headers.forEach((headerText) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });
  tableSorted.appendChild(headerRow);
  footballers.forEach((emp) => {
    let row = document.createElement("tr");
    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });
    tableSorted.appendChild(row);
  });
  myTableSorted.appendChild(tableSorted);
  console.log(myTableSorted.childNodes);
});

// Hide and display sort up and sort down buttons
function displayButtonName() {
  document.getElementById("getSortedAz").style.display = "none";
  document.getElementById("getSortedZa").style.display = "block";
}

function displayButtonNumber() {
  document.getElementById("numberSortedAz").style.display = "none";
  document.getElementById("numberSortedZa").style.display = "block";
}

function displayButtonPosition() {
  document.getElementById("posSortedAz").style.display = "none";
  document.getElementById("posSortedZa").style.display = "block";
}

function displayButtonNationality() {
  document.getElementById("natSortedAz").style.display = "none";
  document.getElementById("natSortedZa").style.display = "block";
}

function displayButtonPrice() {
  document.getElementById("priceSortedAz").style.display = "none";
  document.getElementById("priceSortedZa").style.display = "block";
}

function displayButtonBackName() {
  document.getElementById("getSortedZa").style.display = "none";
  document.getElementById("getSortedAz").style.display = "block";
}

function displayButtonBackNumber() {
  document.getElementById("numberSortedZa").style.display = "none";
  document.getElementById("numberSortedAz").style.display = "block";
}

function displayButtonBackPosition() {
  document.getElementById("posSortedZa").style.display = "none";
  document.getElementById("posSortedAz").style.display = "block";
}

function displayButtonBackNationality() {
  document.getElementById("natSortedZa").style.display = "none";
  document.getElementById("natSortedAz").style.display = "block";
}

function displayButtonBackPrice() {
  document.getElementById("priceSortedZa").style.display = "none";
  document.getElementById("priceSortedAz").style.display = "block";
}
