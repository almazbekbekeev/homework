const array = [];
const btn = document.getElementById("btn");
let result = document.getElementById('result')

function addItem() {
  timeAdd = new Date();

  inputValue = document.getElementById("input-field").value;
  if (inputValue == "") {
    document.getElementById("input-field").style.borderColor = "red";
    return;
  }
  const time = `${timeAdd.getHours()}:${timeAdd.getMinutes()}:${timeAdd.getSeconds()}`;
  const todoItem = { inputValue, time: time };

  array.push(todoItem);
  let item = ` <div class="item-main">
  <div class="item-container">
    <h2 class='item-text'>${inputValue}</h2>
    <h3>${time}</h3>
</div>
<i onclick="remove(this)" class="fa fa-close" style="font-size:48px;color:red"></i>
</div>`;
  
    result.innerHTML += item;

  console.log(array);
  if (array.length >=10) {
    btn.style.display = 'none';
  }

  return false;

}

btn.addEventListener("click", function handleClick(event) {
  event.preventDefault();

  const input = document.getElementById("input-field");

  console.log(input.value);

  input.value = "";
});


function remove(elem) {
  elem.parentNode.remove(elem);
}
