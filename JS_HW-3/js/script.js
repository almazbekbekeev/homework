//#1
function run1() {
  const first = [1, 2, 3];
  const second = [4, 5, 6];
  const unit = first.concat(second);

  console.log(
    "#1 Даны два массива: [1, 2, 3] и [4, 5, 6]. Объедините их вместе."
  );
  console.log(unit);
}

//#2
function run2() {
  const first = [1, 2, 3];
  first.reverse();
  console.log("#2 Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1].");
  console.log(first);
}

//#3
function run3() {
  const first = [1, 2, 3];

  first.push(4, 5, 6);
  console.log(
    "#3 Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6."
  );
  console.log(first);
}
//#4
function run4() {
  const first = [1, 2, 3];

  first.unshift(4, 5, 6);
  console.log(
    "#4 Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6."
  );
  console.log(first);
}

//#5
function run5() {
  const first = ["js", "css", "jq"];
  let firstElement = first.shift();
  console.log(
    "#5 Дан массив ['js', 'css', 'jq']. Выведите в консоли  первый элемент."
  );
  console.log(firstElement);
}

//#6
function run6() {
  const first = ["js", "css", "jq"];
  let lastElement = first.pop();
  console.log(
    "#6 Дан массив ['js', 'css', 'jq']. Выведите в консоли последний элемент."
  );
  console.log(lastElement);
}
//#7
function run7() {
  const numbers = [1, 2, 3, 4, 5];
  const newResult = numbers.slice(0, 3);
  console.log(
    "#7 Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [1, 2, 3]."
  );
  console.log(newResult);
}

//#8
function run8() {
  const numbers = [1, 2, 3, 4, 5];
  const newResult = numbers.slice(3, 5);
  console.log(
    "#8 Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [4, 5]."
  );
  console.log(newResult);
}
//#9
function run9() {
  const numbers = [1, 2, 3, 4, 5];
  numbers.splice(1, 2);
  console.log(
    "#9 Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5]."
  );
  console.log(numbers);
}

//#10
function run10() {
  const numbers = [1, 2, 3, 4, 5];
  let result = numbers.splice(1, 3);
  console.log(
    "#10 Дан массив [1, 2, 3, 4, 5]. С помощью метода splice запишите в новый массив элементы [2, 3, 4]."
  );
  console.log(result);
}
//#11
function run11() {
  const numbers = [1, 2, 3, 4, 5];
  numbers.splice(3, 0, "a", "b", "c");
  console.log(
    "#11 Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 2, 3, 'a', 'b', 'c', 4, 5]."
  );
  console.log(numbers);
}
//#12
function run12() {
  const numbers = [1, 2, 3, 4, 5];
  numbers.splice(1, 0, "a", "b");
  numbers.splice(6, 0, "c");
  numbers.splice(8, 0, "e");
  console.log(
    "#12 Дан массив [1, 2, 3, 4, 5]. С помощью метода splice сделайте из него массив [1, 'a', 'b', 2, 3, 4, 'c', 5, 'e']."
  );
  console.log(numbers);
}

//#13
function run13() {
  const empty = [];
  empty.push("x", "xx", "xxx");
  console.log(
    "#13 Заполните массив следующим образом: в первый элемент запишите 'x', во второй 'xx', в третий 'xxx' и так далее."
  );
  console.log(empty);
}

//#14
function run14() {
  const empty = [];
  empty.push("1", "22", "333");
  console.log(
    "#14 Заполните массив следующим образом: в первый элемент запишите '1', во второй '22', в третий '333' и так далее."
  );
  console.log(empty);
}

//#15
function run15() {
  const notEmpty = [];

  function arrayFill(value, length) {
    for (let index = 1; index <= length; index++) {
      notEmpty.push(value);
    }
  }
  arrayFill("x", 5);
  console.log(
    "#15 Сделайте функцию arrayFill, которая будет заполнять массив заданными значениями. Первым параметром функция принимает значение, которым заполнять массив, а вторым - сколько элементов должно быть в массиве. Пример: arrayFill('x', 5) сделает массив ['x', 'x', 'x', 'x', 'x']"
  );
  console.log(notEmpty);
}

//#16
function run16() {
  const array = [1, 1, 1, 1, 5, 6, 7, 8, 9, 10];
  let sum = 0;

  for (let index = 0; index < array.length; index++) {
    sum += array[index];
    if (sum > 10) {
      console.log(
        "#16 Дан массив с числами. Узнайте сколько элементов с начала массива надо сложить, чтобы в сумме получилось больше 10-ти."
      );
      console.log(
        "Нужно сложить ",
        array[index],
        " элементов, чтобы в сумме получилось больше 10-ти."
      );
      break;
    }
  }
}

//#17
function run17() {
  let result = [];
  function reverse(array) {
    for (let index = array.length - 1; index > -1; index--) {
      result.push(array[index]);
    }
  }
  reverse([1, 2, 4, 6, 7, 8]);
  console.log(
    "#17 Дан массив с числами. Не используя метода reverse переверните его элементы в обратном порядке."
  );
  console.log(result);
}

//#18
function run18() {
  const numbers = [4, 2, 1, 6, 10, 8];
  function compare(a, b) {
    if (a < b) {
      return 1;
    } else if (a > b) {
      return -1;
    } else {
      return 0;
    }
  }
  const sorted = numbers.sort(compare);
  console.log(
    "#18 Дан массив с числами. Найти самое большое число в этом массиве и вывести в консоли."
  );
  console.log(numbers[0]);
}

//#19
function run19() {
  const array = [2, 5, 3, 9];
  let first = array[0] * array[1];
  let second = array[2] * array[3];
  let result = first + second;
  console.log(
    "#19 Создайте массив arr с элементами 2, 5, 3, 9. Умножьте первый элемент массива на второй, а третий элемент на четвертый. Результаты сложите, присвойте переменной result. Выведите на экран значение этой переменной."
  );
  console.log(
    `(${array[0]} * ${array[1]}) + (${array[2]} * ${array[3]}) = ${first} + ${second} = ${result}`
  );
}

//#20

function run20() {
  const array = ["john", "jane", "kate", "peter"];

  for (let index = 0; index < array.length; index++) {
    array[index] = array[index][0].toUpperCase() + array[index].substring(1);
  }
  console.log(
    "#20 Дан массив из имен например ['john', 'jane', 'kate', 'peter']. Вывести новый массив в котором каждая первая буква элемента будет с большой буквы."
  );
  console.log(array);
}
