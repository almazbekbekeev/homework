const knightContainer = document.getElementById("heroContainer");
const bossContainer = document.getElementById("bossContainer");
const roundContainer = document.getElementById("roundContainer");
const maxHealth = 1000;
const maxMana = 200;
const roshanMaxHealth = 5000;

class Round {
  constructor(number) {
    this.number = number;
  }
}

class Hero {
  constructor(name, health, mana, attack, armor) {
    this.name = name;
    this.health = health;
    this.mana = mana;
    this.attack = attack;
    this.armor = armor;
  }
}

class Knight extends Hero {}

const davion = new Hero("Davion", 1000, 200, 100, 5);
const roshan = new Hero("Roshan", 5000, 0, 50, 0);
const roundNumber = new Round(1);
let roshanDamage = roshan.attack - davion.armor;
let knightDamage = davion.attack - roshan.armor;
let spellCost = 60;
let spellCostSuper = maxMana;

let knightSuper = davion.attack * 10;

function countDown(){
  let timeleft = 5;
  document.getElementById('purify').style.display = 'none';
  document.getElementById('countdown').style.display = 'block';
let downloadTimer = setInterval(function(){
  if(timeleft <= 0){
    clearInterval(downloadTimer);
    document.getElementById('countdown').style.display = 'none';
    document.getElementById('purify').style.display = 'block';
  } else {
    document.getElementById("countdown").innerHTML ='Cooldown ' + timeleft;
  }
  timeleft -= 1;
}, 1000);
}

function renderStats(Hero) {
  let empty = "";
  Object.keys(Hero).forEach((key) => {
    empty += `
    <div class = 'stats'>
    <h2>${key}</h2>
    <h2>: ${Hero[key]}</h2>
    </div>
    `;
  });
  return empty;
}

function renderRound(Round) {
  let number = "";
  Object.keys(Round).forEach((key) => {
    number = `
    <h2 class = 'round-number'>Round ${Round[key]}</h2>
    `;
  });
  return number;
}

function reduceArmor() {
  if (roundNumber.number % 6 === 0) {
    davion.armor = davion.armor -1;
  }
}

function updateState() {
  probablility = Math.floor(Math.random() * 34);

  if (davion.mana > spellCost) {
    document.getElementById('purify').style.backgroundColor = '#4caf50';
    document.getElementById('purify').disabled = 'false';
  } 
  if (davion.health <= 0) {
    davion.health = 0;
    alert("Roshan WINS!!!");
  }

  if (davion.mana <= 0) {
    davion.mana = 0;
  }

  if (roshan.health <= 0) {
    roshan.health = 0;
    alert("Davion WINS!!!");
  }

  if (probablility === 5) {
    davion.health = 1;
    alert("ROSHAN INSTANT KILL!!! Only 1 HP left!!!");

  }

  if (davion.mana < spellCost) {
    document.getElementById('purify').style.backgroundColor = 'grey';
    document.getElementById('purify').disabled = 'true';
  } 

  if (davion.mana < spellCostSuper){
    document.getElementById('superPunch').style.backgroundColor = 'grey';
  }

  if (davion.mana >= spellCostSuper){
    document.getElementById('superPunch').style.backgroundColor = '#4caf50';
    document.getElementById('superPunch').disabled = 'false';

  }
  console.log('probablility', probablility);

  reduceArmor();
  bossContainer.innerHTML = renderStats(roshan);
  knightContainer.innerHTML = renderStats(davion);
  roundContainer.innerHTML = renderRound(roundNumber);
}
updateState();

function heal() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  let purification = 300;
  let purified = davion.health + purification;

  if (davion.mana < spellCost) {
    alert('Not enough MP')

  }
  if (davion.health === maxHealth) {
    davion.mana = davion.mana - spellCost;
    davion.health = davion.health - damage;
  } else if (purified > maxHealth) {
    davion.mana = davion.mana - spellCost;
    davion.health = purified - damage;
    console.log(purification);

  } else {
    davion.mana = davion.mana - spellCost;
    davion.health = purified - damage;
  }
  roundNumber.number = roundNumber.number + 1;
  countDown()
  updateState();
}

function attack() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  roshan.health = roshan.health - knightDamage;
  davion.health = davion.health - damage;
  roundNumber.number = roundNumber.number + 1;
  updateState();
  console.log(roundNumber);
  console.log(damage);
}

function superUdar() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  if (davion.mana < spellCostSuper) {
    davion.health = davion.health - damage;
    alert("Not enough mana to use the ability");
  } else {
    davion.mana = davion.mana - maxMana;
    roshan.health = roshan.health - knightSuper;
    davion.health = davion.health - damage;
  }
  roundNumber.number = roundNumber.number + 1;
  console.log(damage);

  updateState();
}

function strangeUdar() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  davion.health = (davion.health / 2) - damage;
  roshan.health = roshan.health / 2;
  document.getElementById('strange').style.backgroundColor = 'grey';
  document.getElementById("strange").onclick = null;

  roundNumber.number = roundNumber.number + 1;
  console.log(damage);

  updateState();
}

function shield() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  davion.armor = 5;
  davion.health = davion.health - damage;
  document.getElementById("shield").style.pointerEvents = "none";
  document.getElementById('shield').style.backgroundColor = 'grey';
  roundNumber.number = roundNumber.number + 1;
  console.log(damage);

  updateState();
}

function restoreMana() {
  damage = (Math.floor(Math.random() * 101) + roshanDamage);
  davion.mana = 200;
  davion.health = davion.health - damage;
  document.getElementById("manaRestore").style.pointerEvents = "none";
  document.getElementById('manaRestore').style.backgroundColor = 'grey';
  roundNumber.number = roundNumber.number + 1;
  console.log(damage);

  updateState();
}
