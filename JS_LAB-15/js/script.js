class User {
  #password;
  constructor(email, password){
    this.email = email
    this.#password = window.btoa(password)
  }

  get pass() {
    return window.atob(this.#password)
  }

  get userEmail() {
    return `this is user email: ${this.email}`
  }
}

class Seller extends User {
  constructor(email, password, rating) {
    super(email, password)
    this.rating = rating
  }
  get userEmail() {
    return `this is seller email: ${this.email}`
  } 
}

const seller = new Seller('seller@mail.com', '123456', 10);
console.log('seller', seller.userEmail);

const user1 = new User('user@mail.com', 'qwerty');
console.log('user', user1.userEmail);