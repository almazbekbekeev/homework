const knightContainer = document.getElementById("heroContainer");
const bossContainer = document.getElementById("bossContainer");

class Hero {
  constructor(name, health, mana, attack, armor) {
    this.name = name;
    this.health = health;
    this.mana = mana;
    this.attack = attack;
    this.armor = armor;
  }
}

class Knight extends Hero {}

const davion = new Hero("Davion", 1000, 200, 100, 5);
const roshan = new Hero("Roshan", 5000, 0, 50, 0);

function renderStats(Hero) {
  let empty = "";
  Object.keys(Hero).forEach((key) => {
    empty += `
    <div class = 'stats'>
    <h2>${key}</h2>
    <h2>: ${Hero[key]}</h2>
    </div>
    `;
  });
  return empty;
}

function updateState() {
  bossContainer.innerHTML = renderStats(roshan);
  knightContainer.innerHTML = renderStats(davion);
}
updateState();

function heal() {
  if (davion.health === 1000) {
    alert("Your knight HP is full, no need to heal");
  } else if (davion.health + 100 > davion.maxHealth) {
    davion.health = davion.maxHealth;
  } else {
    davion.health = davion.health + 100;
  }
  updateState();
}

function shield() {
  davion.armor = davion.armor + 30;
  updateState();
}

function ultimate() {
  davion.attack = davion.attack * 3;
  davion.mana = davion.mana - 50;
  updateState();
}
