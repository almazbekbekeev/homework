// let promise = new Promise((resolve, reject) => {
// setTimeout(() => {
//   // resolve('Resolved');
//   reject('Something went wrong')
// }, 1000);
// });
// promise.then((result) => {
//   console.log('result', result);
// }).catch((error) => {
//   console.log('error', error);
// })
const usersContainer = document.getElementById('usersContainer');
const postsContainer = document.getElementById('postsContainer');


const renderUsers = (users) => {
  console.log("users", users);
  let result = "";
  users.forEach((element) => {
    result += `
    <div>
    <div>UserID: ${element.id}</div>
    <div>Username: ${element.username}</div>
    <div>Email: ${element.email}</div>
    <div>Phone: ${element.phone}</div>
    <div>Website: ${element.website}</div>
    </div>
    `
  });
  usersContainer.innerHTML = result;
};

const posts = (posts) => {
  console.log("posts", posts);
  let result = "";
  users.forEach((element) => {
    result += `
    <div>
    <div>UserID: ${element.title}</div>
    <div>Username: ${element.body}</div>
    </div>
    `
  });
  postsContainer.innerHTML = result;
};

const fetchData = () => {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      return response.json();
    })
    .then((users) => {
      renderUsers(users);
    })
    .catch((error) => {
      console.log("error", error);
    });
};

const fetchPosts = () => {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => {
      return response.json();
    })
    .then((posts) => {
      renderPosts(posts);
    })
    .catch((error) => {
      console.log("error", error);
    });
}