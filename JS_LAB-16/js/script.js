console.log('local storage', localStorage.getItem('array'));
const array = [1, 2, 3];
const obj = {name: 'John', age:80}

const handleButton = () => {
  array.push(4);
  const jsonObj = JSON.stringify(obj);

  localStorage.setItem('array', array);
  localStorage.setItem('object', jsonObj);

  console.log('array', localStorage.getItem('array'));
  const getObject = localStorage.getItem('object')
  const newObj = JSON.parse(getObject);

  console.log('array', localStorage.getItem('array'));
  console.log(('object', newObj.name));
}
// console.log('array', array);

const timeout = () => {
  console.log('run');
  setTimeout(() => {
console.log('after timeout');
  }, 5000)
}
