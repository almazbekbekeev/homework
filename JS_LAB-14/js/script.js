const container = document.getElementsByClassName("container")[0];

class Product {
  constructor(name, price, quantity, discount) {
    this.name = name
    this.price = price
    this.quantity = quantity
    this.discount = discount
  }
  
  get discountPrice() {
    return this.price - this.price * this.discount;
  }

  set disc(value) {
    this.discount = value.disc
    this.price = value.price
  }
}

class Shoe extends Product {
constructor(name, price, quantity, size, color) {
  super(name, price, quantity, 0)
  this.size = size
  this.color = color
}
}
  
const nike = new Product("Nike", 150, 100, 0.2);
const adidas = new Product("Adidas", 100, 200, 0.5);
// nike.price = 250;
nike.disc = {disc: 0.5, price: 200};
const sketchers = new Shoe('Sketchers', 500, 1, 42, 'white')
console.log('Sketchers', sketchers);

const items = [nike, adidas, sketchers];
console.log(nike);

const renderProducts = (products) => {
  let result = "";

  products.forEach((element) => {
    result = `<div>
    name: ${element.name}, 
    price: ${element.price} $, 
    quantity: ${element.quantity},
    price for sale: ${element.discountPrice}$,
    ${element.size ? `size: ${element.size} ` : ''},
    ${element.color ? `color: ${element.color}` : ''}
    </div>`
    container.innerHTML += result;
  });
};

renderProducts(items);
