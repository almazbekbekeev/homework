//#1
function run1() {
  const array = ["john", "jane", "kate", "peter"];

  for (let index = 0; index < array.length; index++) {
    array[index] = array[index][0].toUpperCase() + array[index].substring(1);
  }
  console.log(
    "#20 Дан массив из имен например ['john', 'jane', 'kate', 'peter']. Вывести новый массив в котором каждая первая буква элемента будет с большой буквы."
  );
  console.log(array);
}

//#2
function run2() {
  const array = [
    "Hello",
    "My name is Nurbek",
    "London is a capital of Great Britain",
  ];
  const input = document.getElementById("search-input");
  const inputValue = input.value.toLowerCase();
  const lower = array.map((element) => element.toLowerCase());
  filtered = lower.filter(function (str) {
    return str.includes(inputValue);
  });
  console.log(
    `Задача 2. На html странице должен быть input. написать функцию которая достает значение введенного текста и делает поиск по ней не чувствительно к регистру.  Пример запроса 'Hello' или 'hello' должен возвращать всю строку, если нет совпадений тогда возвращает сообщение о том что ничего не найдено. Строка может быть любая, запрос может быть любой.`
  );
  if (inputValue !== "" && filtered.length > 0) {
    console.log("Your input is:", inputValue);
    console.log("Result: ", filtered);
  } else if (filtered.length === 0) {
    console.log("Your input is:", inputValue);
    console.log("Result: No matches found");
  } else {
    console.log("No text entered!");
  }
}

//#3
function run3() {
  const input = document.getElementById("count-input");
  const inputValue = input.value.toLowerCase();

  let counter = (inputValue) => {
    return inputValue.split("").reduce((total, letter) => {
      total[letter] ? total[letter]++ : (total[letter] = 1);
      return total;
    }, []);
  };
  console.log(`На html странице должен быть input. Написать функцию которая считает кол-во каждой буквы в данной строке и выводит в консоль. 
input:  "hello world" 
Выводит в html output: h: 1, e: 1, l: 3, o: 2, w: 1, r: 1, d: 1 `);
  console.log(counter(inputValue));
}

//#4 Сумма диапазона. Напишите функцию range, принимающую два аргумента, начало и конец диапазона, и возвращающую массив, который содержит все числа из него, включая начальное и конечное. Затем напишите функцию sum, принимающую массив чисел и возвращающую их сумму. Запустите указанную выше инструкцию и убедитесь, что она возвращает сумму значений массива.
function range(start, end) {
  const array = [];

  while (start <= end) {
    array.push(start);
    start++;
  }
  console.log(
    `Сумма диапазона. Напишите функцию range, принимающую два аргумента, начало и конец диапазона, и возвращающую массив, который содержит все числа из него, включая начальное и конечное. Затем напишите функцию sum, принимающую массив чисел и возвращающую их сумму. Запустите указанную выше инструкцию и убедитесь, что она возвращает сумму значений массива.`
  );
  console.log(array);

  const sum = array.reduce((accumulator, number) => {
    return accumulator + number;
  }, 0);
  console.log(sum);
}

//#5

function rangeWithStep(start, end, step) {
  const array = [];

  if (step > 0) {
    
  for (let index = start; index <= end; index += step) {
    const element = array[index];
    array.push(index);
    }
  }
else if (step == null && start < end){
  for (let index = start; index <= end; index++) {
    const element = array[index]; 
    array.push(index);
    }
}

else if (step == null && start > end){
  for (let index = start; index >= end; index--) {
    const element = array[index];
    array.push(index);
    }
}

else {
  for (let index = start; index >= end; index += step) {
    const element = array[index];
    array.push(index);
    }
  }
  console.log(
    `Дополните функцию range, чтобы она могла принимать необязательный третий аргумент – шаг для построения массива. Если он не задан, шаг равен единице. Вызов функции range(1, 10, 2) должен будет вернуть [1, 3, 5, 7, 9]. Убедитесь, что она работает с отрицательным шагом так, что вызов range(5, 2, -1) возвращает [5, 4, 3, 2].`
  );
  console.log(array);

  const sum = array.reduce((accumulator, number) => {
    return accumulator + number;
  }, 0);
  console.log(sum);
}


